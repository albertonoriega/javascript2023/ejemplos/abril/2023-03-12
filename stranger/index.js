// Si ponemos el script antes del body, tenemos que usar .addEventListener('load') para que cargue la página primero
// Como hemos puesto el script al final, no hace falta
//window.addEventListener("load", (e) => {

//Constante que apunta a la musica
const musica = document.querySelector('audio');
//Constante que apunta a la foto de la letras
const img = document.getElementById("letras");

// Array que precarga todas las imagenes que se muestran al pulsar las teclas 
const imgArray = [];

//Inicializo el array con las imagenes
// Del 65 al 91 son los números del método keyCode que corresponden a las letras de la A a la Z
// 65 => A , 66=> B
// Creamos un array de imágenes
for (i = 65; i < 91; i++) {
    // Hacemos que el array de las imágenes empiece en 0
    imgArray[i - 65] = new Image();
    imgArray[i - 65].src = "imgs/" + i + ".gif";
}



//CONTROL DE EVENTOS

//cuando pulso la tecla
document.addEventListener("keydown", (evento) => {
    //Constante que apunta a la foto de la letras
    const img = document.getElementById("letras");

    // Guardamos en la variable codigo el codigo de la tecla pulsada
    let codigo = evento.keyCode;
    // Si se ha pulsado un tecla de la A a la Z => muestra la imagen correspondiente a la tecla 
    if (codigo > 64 && codigo < 91) {
        img.src = imgArray[codigo - 65].src;
    }
    // Quiero que la música se pare
    musica.pause();
});
//Cuando suelto la tecla
document.addEventListener("keyup", (evento) => {
    //Constante que apunta a la foto de la letras
    const img = document.getElementById("letras");
    //Constante que apunta a la musica
    const musica = document.querySelector('audio');

    // Coloco la imagen de todas las bombillas apagadas
    img.src = "imgs/vacio.gif";

    //musica.currentTime+=10;
    musica.playbackRate += .1;
    musica.play();
});

//});