// Crear un formulario para introducir el nombre y la edad
//Crear un objeto con los datos introducidos en el formulario
// Crear metodo mostrar para que escribe los datos en el div de clase salida
let boton = document.querySelector('button');

// Creo un array para almacenar todas las personas
const personas = [];

boton.addEventListener('click', (e) => {
    let inputNombre = document.querySelector('#nombre');
    let inputApellidos = document.querySelector('#apellidos');
    let inputEdad = document.querySelector('#edad');

    const persona = {
        //propiedades
        nombre: "",
        apellidos: "",
        edad: 0,
        // métodos
        nombreCompleto: function () {
            return this.nombre + " " + this.apellidos;
        },
        // metodo con argumento
        mostrar: function (nombre) {
            let cajaSalida = document.querySelector(nombre);
            cajaSalida.innerHTML += this.nombreCompleto() + "<br>";
        },
    };
    persona.nombre = inputNombre.value;
    persona.apellidos = inputApellidos.value;
    persona.edad = inputEdad.value;

    personas.push(persona);

    // mostramos el metodo en la consola
    console.log(persona.nombreCompleto());

    //llamar al metodo que me muestra en la cajaSalida
    persona.mostrar('.salida');

});