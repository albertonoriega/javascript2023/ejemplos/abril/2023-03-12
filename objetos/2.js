// Crear un formulario para introducir el nombre y la edad
//Crear un objeto con los datos introducidos en el formulario
let boton = document.querySelector('button');

// Creo un array para almacenar todas las personas
const personas = [];

boton.addEventListener('click', (e) => {
    let inputNombre = document.querySelector('#nombre');
    let inputApellidos = document.querySelector('#apellidos');
    let inputEdad = document.querySelector('#edad');

    const persona = {
        //propiedades
        nombre: "",
        apellidos: "",
        edad: 0,
        // métodos
        nombreCompleto: function () {
            return this.nombre + " " + this.apellidos;
        },
    };
    persona.nombre = inputNombre.value;
    persona.apellidos = inputApellidos.value;
    persona.edad = inputEdad.value;

    personas.push(persona);

    // mostramos el metodo en la consola
    console.log(persona.nombreCompleto());

});