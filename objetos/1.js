// Crear un formulario para introducir el nombre y la edad
//Crear un objeto con los datos introducidos en el formulario
let boton = document.querySelector('button');

// Creo un array para almacenar todas las personas
const personas = [];

boton.addEventListener('click', (e) => {
    let inputNombre = document.querySelector('#nombre');
    let inputEdad = document.querySelector('#edad');

    const persona = {
        nombre: "",
        edad: 0,
    };
    persona.nombre = inputNombre.value;
    persona.edad = inputEdad.value;

    console.log(persona);
    personas.push(persona);

});